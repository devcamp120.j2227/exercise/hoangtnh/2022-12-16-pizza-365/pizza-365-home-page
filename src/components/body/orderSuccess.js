import { Grid } from "@mui/material"
import { Container } from "@mui/system"
import TextField from '@mui/material/TextField';
import { useSelector } from "react-redux";

const SubmitSuccessOrder = () =>{
    const {orderId} = useSelector((reduxData)=>{
        return reduxData.taskReducer;
    });
    return (
    <div className="order-success">
        <Container>
            <Grid container >
                <Grid item xs={12}>
                    <h3>Cảm ơn quý khách đã chọn pizza 365</h3>
                </Grid>
                <Grid container spacing={0}>
                    <Grid item xs={3}>
                        <p>Mã đơn hàng của quý khách là: </p>
                    </Grid>
                    <Grid item xs={3}>
                        <TextField
                            disabled
                            id="outlined-disabled"
                            defaultValue= {`${orderId}`}
                            fullWidth
                        />
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    </div>
        
    )
}
export default SubmitSuccessOrder;