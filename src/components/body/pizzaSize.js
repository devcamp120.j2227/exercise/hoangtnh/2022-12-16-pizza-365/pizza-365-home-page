import { Grid, Card, CardHeader, CardContent, Typography, CardActions } from "@mui/material";
import { Container } from "@mui/system";
import { useDispatch, useSelector } from "react-redux";
import ButtonStyle from "../styleComponent/styleButton";
import { PizzaSizeHandler } from "../../actions/task.action";
const PizzaSize = () =>{
    //Khai báo dispatch để lấy giá trị
    const dispatch = useDispatch()
    //B1: nhận giá trị khởi tạo của state trong giai đoạn đầu
    const {pizzaSize} = useSelector((reduxData)=>{
        return reduxData.taskReducer;
    });
    const onButtonPizzaSize = (id) =>{
        dispatch(PizzaSizeHandler(id));
    }
    return(
        <Container>
            <Grid   container 
                    justifyContent="center" 
                    marginTop="50px" 
                    marginBottom="20px" 
                    direction="column" 
                    alignItems="center">
                <Grid item className="brand">
                    <h2>
                        CHỌN SIZE PIZZA
                    </h2>
                    <hr className="title-hr"/>
                </Grid>
                <Grid item className="brand">
                    <p>Chọn combo pizza phù hợp với nhu cầu của bạn.</p>
                </Grid>
            </Grid>
            <Grid container spacing={10} marginBottom="20px">
            {pizzaSize.map((value,index)=>{
                return (
                <Grid item xs={4} key={index}>
                    <Card>
                        <CardHeader className="header-card"
                                subheader={
                                <CardActions>
                                    <Grid 
                                    container
                                    direction="row"
                                    justifyContent="center"
                                    alignItems="center">
                                    <h2>{value.size} ({value.decription})</h2>
                                    </Grid>
                                </CardActions>
                            }/>
                            <CardContent>
                                {
                                <Typography variant="body">
                                    <Grid
                                    container
                                    direction="column"
                                    justifyContent="center"
                                    alignItems="center">
                                        <Grid item>
                                            <p>Đường kính: <b>{value.duongKinh}</b></p>
                                        </Grid>
                                            <hr className="card-hr"/>
                                        <Grid item>
                                            <p> Sườn nướng: <b>{value.suonNuong}</b></p>
                                        </Grid>
                                            <hr className="card-hr"/>
                                        <Grid item>
                                            <p> Salad: <b>{value.salad}</b></p>
                                        </Grid>
                                            <hr className="card-hr"/>
                                        <Grid item>
                                            <p> Nước ngọt: <b>{value.drink}</b></p>
                                        </Grid>
                                            <hr className="card-hr"/>
                                        <Grid container alignItems="center" direction="column">
                                            <Grid item>
                                                <h1>{value.price}</h1>
                                            </Grid>
                                            <Grid item>
                                                <p style={{margin:"0px"}}>VNĐ</p>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Typography>
                                }
                            </CardContent>
                            <CardHeader className="footer-card"
                                subheader={
                                <CardActions>
                                    <Grid 
                                    container
                                    direction="row"
                                    justifyContent="center"
                                    alignItems="center">
                                        <ButtonStyle variant="contained" size="medium"
                                        onClick={()=>onButtonPizzaSize(index)}>Chọn</ButtonStyle>
                                    </Grid>
                                </CardActions>
                            }/>
                        </Card>
                </Grid>
                ) })}
            </Grid>
        </Container>
    )
}
export default PizzaSize;