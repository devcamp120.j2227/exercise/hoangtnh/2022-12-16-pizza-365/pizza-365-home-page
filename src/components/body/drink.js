import { Grid, Box, InputLabel, Select, MenuItem, FormControl } from "@mui/material";
import { Container } from "@mui/system"
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DrinkHandler } from "../../actions/task.action";

const DrinkSelect = () =>{
    //Khai báo dispatch để lấy giá trị
    const dispatch = useDispatch()
    const [drink, setDrink] = useState("");

    const onChangeDrinkHandler = (event) =>{
        setDrink(event.target.value);
        dispatch(DrinkHandler(event.target.value));
    }
    return (
        <Container>
            <Grid   container 
                    justifyContent="center" 
                    marginTop="50px" 
                    marginBottom="20px" 
                    direction="column" 
                    alignItems="center">
                <Grid item className="brand">
                    <h2>
                        CHỌN ĐỒ UỐNG
                    </h2>
                    <hr className="title-hr"/>
                </Grid>
            </Grid>
            <Box sx={{ minWidth: 120 }}>
                <FormControl fullWidth>
                    <InputLabel>Tất cả loại nước uống</InputLabel>
                    <Select label="Tất cả loại nước uống" 
                            onChange={onChangeDrinkHandler}
                            value={drink}>
                        <MenuItem value={"Trà Tắc"}>Trà Tắc</MenuItem>
                        <MenuItem value={"Coca"}>Coca Cola</MenuItem>
                        <MenuItem value={"Pepsi"}>Pepsi</MenuItem>
                        <MenuItem value={"Lavie"}>Lavie</MenuItem>
                        <MenuItem value={"Trà Sữa Trân Châu"}>Trà Sữa Trân Châu</MenuItem>
                        <MenuItem value={"Fanta"}>Fanta</MenuItem>
                    </Select>
                </FormControl>
            </Box>
        </Container>
    );
}
export default DrinkSelect;