import { Grid } from "@mui/material";
import { Container } from "@mui/system"
import StyleBox from "../styleComponent/styleBox";
const Info =()=>{
    return(
        <Container>
            <Grid container justifyContent="center" marginTop="50px" marginBottom="20px">
                <Grid item className="brand">
                <h2>
                    TẠI SAO LẠI PIZZA 365
                </h2>
                <hr className="title-hr"/>
                </Grid>
            </Grid>
            <Grid container direction="row">
                <Grid item xs={3}>
                    <StyleBox  bg="#fafad2">
                        <h3>Đa dạng</h3>
                        <p>Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot hiện nay.</p>
                    </StyleBox>
                </Grid>
                <Grid item xs={3}>
                    <StyleBox bg="#ffff00">
                        <h3>Chất lượng</h3>
                        <p>Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</p>
                    </StyleBox>
                </Grid>
                <Grid item xs={3}>
                    <StyleBox bg="#ffa07a">
                        <h3>Hương vị</h3>
                        <p>Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.</p>
                    </StyleBox></Grid>
                <Grid item xs={3}>
                    <StyleBox bg="#ffa500">
                        <h3>Dịch vụ</h3>
                        <p>Nhân viên thân thiện, nhà hàng hiện đại, Dịch vụ giao hàng nhanh chất lượng, tân tiến.</p>
                    </StyleBox>
                </Grid>
                
                
                
            </Grid>
        </Container>
    )
}
export default Info;