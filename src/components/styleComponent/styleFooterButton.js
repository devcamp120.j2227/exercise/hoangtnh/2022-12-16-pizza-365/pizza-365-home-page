import { styled } from '@mui/material/styles';
import Button from "@mui/material/Button";

const ButtonFootertyle = styled(Button)({
    marginBottom:"15px",
    boxShadow: "none",
    textTransform: "none",
    fontSize: 19,
    color:"white",
    padding: "6px 12px",
    lineHeight: 1.5,
    backgroundColor: "#343a40",
    color: "white", 
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    "&:hover": {
      backgroundColor: "#343a40",
      filter: "brightness(120%)",
      boxShadow: "none"
    },
    "&:active": {
      boxShadow: "none",
      backgroundColor: "none",
      border: "none"
    }
  });
  export default ButtonFootertyle;