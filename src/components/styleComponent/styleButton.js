import { styled } from '@mui/material/styles';
import Button from "@mui/material/Button";
const ButtonStyle = styled(Button)({
    boxShadow: "none",
    textTransform: "none",
    fontSize: 19,
    padding: "6px 12px",
    lineHeight: 1.5,
    backgroundColor: "orange",
    width: "100%",
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    "&:hover": {
      backgroundColor: "orange",
      filter: "brightness(120%)",
      boxShadow: "none"
    },
    "&:active": {
      boxShadow: "none",
      backgroundColor: "none",
      border: "none"
    }
  });
  export default ButtonStyle;