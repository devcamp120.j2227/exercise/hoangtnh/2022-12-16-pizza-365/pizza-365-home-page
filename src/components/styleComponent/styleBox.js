import styled from "styled-components";

const StyleBox = styled.div`
background-color: ${props => props.bg};
padding: 15px;
border: 1px solid #ffb630;
height: 150px
`;
export default StyleBox;