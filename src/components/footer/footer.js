import { Grid } from "@mui/material"
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'font-awesome/css/font-awesome.min.css';
import ButtonFootertyle from "../styleComponent/styleFooterButton";
const Footer = () =>{
    return (
        <Grid container className="footer"
        direction="column"
        justifyContent="center"
        alignItems="center">
            <h2>Footer</h2>
            <ButtonFootertyle>
                To the top
            </ButtonFootertyle>
            <Grid> <i className="fa-brands fa-facebook fa-lg"/> <i className="fa-brands fa-instagram fa-lg"/> <i className="fa-brands fa-square-snapchat fa-lg"/> <i className="fa-brands fa-pinterest-p fa-lg"/> <i className="fa-brands fa-twitter fa-lg"/></Grid>
            <p>Powered by DEVCAMP</p>
        </Grid>
    )
}
export default Footer;