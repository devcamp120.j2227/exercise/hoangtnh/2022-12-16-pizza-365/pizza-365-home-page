import SliderCaroul from "./body/carousel";
import DrinkSelect from "./body/drink";
import Info from "./body/infor";
import SubmitSuccessOrder from "./body/orderSuccess";
import PizzaSize from "./body/pizzaSize";
import PizzaType from "./body/pizzatype";
import SubmitForm from "./body/submitForm";
import Footer from "./footer/footer";
import Header from "./header/header";
import { useSelector } from "react-redux";

const Page = () =>{
    const {orderId} = useSelector((reduxData)=>{
        return reduxData.taskReducer;
    });
    return (
        <>
            <Header/>
            <SliderCaroul/>
            <Info/>
            <PizzaSize/>
            <PizzaType/>
            <DrinkSelect/>
            <SubmitForm/>
            {orderId !== null?<SubmitSuccessOrder/>:null}
            <Footer/>
        </>
    )
}
export default Page;