import {
    TASK_PIZZA_SIZE_HANDLER , 
    TASK_PIZZA_TYPE_HANDLER, 
    TASK_DRINK_HANDLER, 
    TASK_FORM_HANDLER, 
    TASK_SUBMIT_HANDLER,
    TASK_SEND_ORDER
} from "../constants/task.constants";

const PizzaSizeHandler = (id) =>{
    return {
        type: TASK_PIZZA_SIZE_HANDLER,
        payload: id
    }
}
const PizzaTypeHandler = (id)=>{
    return {
        type: TASK_PIZZA_TYPE_HANDLER,
        payload: id
    }
}
const DrinkHandler = (value) =>{
    return {
        type: TASK_DRINK_HANDLER,
        payload: value
    }
}
const FormHandler = (value) =>{
    return{
        type: TASK_FORM_HANDLER,
        payload: value
    }
}
const SubmitFormHandler = ()=>{
    return{
        type: TASK_SUBMIT_HANDLER
    }
}
const SendOrderHandler = (value) =>{
    return {
        type: TASK_SEND_ORDER,
        payload: value
    }
}
export {
    PizzaSizeHandler,
    PizzaTypeHandler,
    DrinkHandler,
    FormHandler,
    SubmitFormHandler,
    SendOrderHandler
}